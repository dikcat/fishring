﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CircleFishing
{
    abstract class GameObject
    {

        public abstract void update();
        public abstract void updateForces();
    }
}
