﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace CircleFishing
{
    class bubbleVertex : GameObject
    {
        /// <summary>
        /// some arb values for the springs
        /// </summary>
        
        private const float k = 0.5f; //spring coefficient
        private const float mass = 10f;  //mass off the ass
        private const float damp = 0.25f; //damping coefficient

        /// <summary>
        /// Forces and Accelerations for the verts
        /// </summary>

        private float radialAcceleration;
        private float radialVelocity;
        private float externalForce;
        private float restRadius;

        /// <summary>
        /// angle and radius of the current vertex
        /// </summary>

        public float radialForce { get; private set; }
        public float currentRadius { get; private set; }

        private float angle;

        /// <summary>
        /// Methods
        /// </summary>
        
        //constructor
        public bubbleVertex(float angle,float radius) {
            this.angle = angle;
            restRadius = radius;
            currentRadius = restRadius;
        }

        //MADNESS
        public void madness()
        {
            currentRadius = restRadius + 50;
        }

        //get the position vertices as a Vector2
        public Vector2 getPosition()
        {
            return new Vector2((float)Math.Sin(angle) * currentRadius, (float)Math.Cos(angle) * currentRadius);
        }

        /// <summary>
        /// Physics Methods
        /// </summary>

        public override void updateForces()
        {
            radialForce = -(k * (currentRadius - restRadius)) -(damp * radialVelocity);
        }

        public void normalise(float leftForce, float rightForce)
        {
            externalForce = (leftForce * 0.45f) + (rightForce * 0.45f);
        }

        public override void update()
        {
            radialForce -= externalForce;
            radialAcceleration = radialForce / mass;
            radialVelocity += radialAcceleration;
            currentRadius += radialVelocity;
        }
    }
}
