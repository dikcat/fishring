﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace CircleFishing
{
    abstract class Screen
    {
        public abstract void show(); //initialize stuff

        public abstract void update(); //update stuff

        public abstract void render(); //draw/render stuff

        public abstract void hide(); //kill stuff


    }
}
