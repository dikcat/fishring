﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace CircleFishing.Screens
{
    class ScreenMainMenu : Screen
    {
        private Game game;

        /// <summary>
        /// constructor
        /// </summary>
        public ScreenMainMenu(Game game)
        {
            this.game = game;
        }

        public override void show()
        {
            Console.Write("Show me thy melons!\n");
        }

        public override void update()
        {
            //temp code 
            var state = OpenTK.Input.Keyboard.GetState();
            if (state.IsKeyDown(OpenTK.Input.Key.Enter))
            {
                Console.Write("This is broken af\n");
                //need to fix input so it's not cray hue pressed I will keep going till you release
                //set screen to ScreenGame
                game.setScreen(new Screens.ScreenGame(game));
                //help
            }
            //temp code end

        }

        public override void render()
        {
            GL.Begin(PrimitiveType.Quads);
            GL.Vertex2(0,0);
            GL.Vertex2(10,0);
            GL.Vertex2(10,10);
            GL.Vertex2(0,10);
            
            GL.End();            
        }

        public override void hide()
        {
            Console.Write("Killed Main Menu\n");
        }

    }
}
