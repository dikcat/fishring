﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using System.Drawing;

namespace CircleFishing.Screens
{
    class ScreenGame : Screen 
    {
        List<bubbleVertex> verts = new List<bubbleVertex>();
        List<GameObject> gameObjectList = new List<GameObject>();

        private Game game;
       
        /// <summary>
        /// constructor
        /// </summary>
        public ScreenGame(Game game)
        {
            this.game = game;
        }

        public override void show()
        {
            Console.Write("Show me thy grapes!");
            float angle = 0.0f;
            float radius = 200f;
            int points = 128;
            float increment = (float)Math.PI * 2.0f / points;

            for (angle = 0; angle < 2 * Math.PI; angle += increment)
            {
                bubbleVertex bubbleVertex = new bubbleVertex(angle, radius);
                verts.Add(bubbleVertex);
                gameObjectList.Add(bubbleVertex);
            }

            verts[0].madness();
        }

        public override void update()
        {
            foreach (GameObject gameObject in gameObjectList)
                gameObject.updateForces();

            for (int i = 0; i < verts.Count; i++)
            {
                bubbleVertex leftVertex;
                bubbleVertex rightVertex;

                if (i == 0)
                    rightVertex = verts[verts.Count - 1];
                else
                    rightVertex = verts[i - 1];

                if (i == verts.Count - 1)
                    leftVertex = verts[0];
                else
                    leftVertex = verts[i + 1];

                verts[i].normalise(leftVertex.radialForce, rightVertex.radialForce);
            }

            foreach (GameObject gameObject in gameObjectList)
                gameObject.update();
        }

        public override void render()
        {
            _drawBubble();
        }

        public override void hide()
        {

        }

        /// <summary>
        /// Draw functions
        /// </summary>
        public void _drawBubble()
        {
            GL.Begin(PrimitiveType.Polygon);
            for (int drawbubblecounter = 0; drawbubblecounter < verts.Count(); drawbubblecounter++)
            {
                GL.Color4(Color.FromArgb(60, Color.DarkSeaGreen));
                GL.Vertex2(verts[drawbubblecounter].getPosition());
            }
            GL.End();
            GL.LineWidth(1.2f);
            GL.Begin(PrimitiveType.LineLoop);
            for (int drawbubblecounter = 0; drawbubblecounter < verts.Count(); drawbubblecounter++)
            {
                GL.Color4(Color.Wheat);
                GL.Vertex2(verts[drawbubblecounter].getPosition());
            }
        }
    }
}
