﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;


namespace CircleFishing
{
    class Game : GameWindow
    {
        View view;
        Screen screen;

        public void initialise()
        {
        }

        public Game(int width, int height)
            :base(width, height)
        {
            view = new View(Vector2.Zero, 1f, 0);
        }
        
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            GL.Enable(EnableCap.Blend);
            GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);

            initialise();
        }

        protected override void OnResize(EventArgs e)
        {
            // Standard OpenTK code for window resize
            base.OnResize(e);

            GL.Viewport(0, 0, Width, Height);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
            view.update();
            screen.update();
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);
            GL.Clear(ClearBufferMask.ColorBufferBit);
            GL.ClearColor(Color.FromArgb(77,171,110));
            GL.LoadIdentity();
            GL.Ortho(-(float)Width / 2, (float)Width / 2, -(float)Height / 2, (float)Height / 2, 0.0, 4.0);
            view.ApplyTransform();

            //_drawBubble(verts);
            screen.render();
            GL.End();
            SwapBuffers();
        }

        /// <summary>
        /// other functions
        /// </summary>
        
        public void setScreen(Screen screen)
        {
            /*
            if (screen != null)
            {
                this.screen.hide(); //destroyes the earlier screen
            }
            */ //Will use after we get more screens^

            this.screen = screen; //sets the screen
            this.screen.show(); //initializes the screen
        }
       

    }
}